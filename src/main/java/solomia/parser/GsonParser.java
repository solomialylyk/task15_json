package solomia.parser;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solomia.comparator.SweetComparator;
import solomia.model.Sweet;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GsonParser {
    private static Logger log = LogManager.getLogger(GsonParser.class);
    private SweetComparator comparator;

    public GsonParser() {
        comparator = new SweetComparator();
    }

    public List<Sweet> parse(File jsonFile) {
        Type listType = new TypeToken<ArrayList<Sweet>>() {
        }.getType();

        List<Sweet> candies = null;
        try {
            candies = new Gson().fromJson(new FileReader(jsonFile), listType);
            candies.sort(comparator);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return candies;
    }
}

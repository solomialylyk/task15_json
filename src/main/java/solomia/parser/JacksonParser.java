package solomia.parser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solomia.comparator.SweetComparator;
import solomia.model.Sweet;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JacksonParser {
    private static Logger log = LogManager.getLogger(JacksonParser.class);
    private SweetComparator comparator;

    public JacksonParser() {
        comparator = new SweetComparator();
    }

    public List<Sweet> parse(File jsonFile) {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Sweet> candyList = null;
        try {
            candyList = objectMapper.readValue(jsonFile, new TypeReference<List<Sweet>>() {
            });
            candyList.sort(comparator);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return candyList;
    }
}

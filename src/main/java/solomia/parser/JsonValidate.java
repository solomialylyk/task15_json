package solomia.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import solomia.controller.ParserControllerImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JsonValidate {
    private static Logger log = LogManager.getLogger(JsonValidate.class);

    public boolean isValid(File jsonPath, File jsonSchemaPath) {
        try (InputStream inputStream = new FileInputStream(jsonSchemaPath)) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            Schema schema = SchemaLoader.load(rawSchema);
            schema.validate(new JSONArray(new JSONTokener(new FileInputStream(jsonPath))));
        } catch (IOException | ValidationException e) {
            log.error(e.getMessage());
        }
        return true;
    }
}

package solomia.view;

public interface Printable {
    void print();
}

package solomia.comparator;

import solomia.model.Sweet;

import java.util.Comparator;

public class SweetComparator implements Comparator<Sweet> {
    public int compare(Sweet o1, Sweet o2) {
        return o1.getEnergy()-o2.getEnergy();
    }
}

package solomia.controller;

public interface Controller {
    void parseGson();

    void parseJackson();
}

package solomia.model;

import java.util.ArrayList;
import java.util.List;

public class Sweet {
    private int num;
    private String name;
    private int energy;
    private String type;
    private List<Ingredient> ingredients = new ArrayList<Ingredient>();
    private Value value;
    private String production;

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public Sweet(int num, String name, int energy, String type, List<Ingredient> ingredients, Value value, String production) {
        this.num = num;
        this.name = name;
        this.energy = energy;
        this.type = type;
        this.ingredients = ingredients;
        this.value = value;
        this.production=production;
    }

    public Sweet(String name) {
        this.name = name;
    }

    public Sweet() {

    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Sweet{" +
                "num=" + num +
                ", name='" + name + '\'' +
                ", energy=" + energy +
                ", type='" + type + '\'' +
                ", ingredients=" + ingredients +
                ", value=" + value +
                '}'+'\n';
    }
}

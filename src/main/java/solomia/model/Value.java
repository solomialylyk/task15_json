package solomia.model;

public class Value {
    private double proteins;
    private double carbohydrates;
    private double fats;

    public Value() {
    }

    public Value(double proteins, double carbohydrates, double fats) {
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.fats = fats;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public double getFats() {
        return fats;
    }

    public void setFats(double fats) {
        this.fats = fats;
    }

    @Override
    public String toString() {
        return "Value{" +
                "proteins='" + proteins + '\'' +
                ", carbohydrates='" + carbohydrates + '\'' +
                ", fats='" + fats + '\'' +
                '}';
    }
}
